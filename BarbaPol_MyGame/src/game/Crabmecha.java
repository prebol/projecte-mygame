package game;

import Core.Field;
import Core.Sprite;
import Core.Window;

import java.util.Random;

import java.awt.Color;

public class Crabmecha extends Boss {
	
	Random r = new Random();
	
	PersonalTimerTask ataque1 = new PersonalTimerTask() {
		
		public void run () {
			
			
//			if (direccion.equals("R")) {
//				addForce(1.005, 0);
//			}
//			
//			if (direccion.equals("L")) {
//				addForce(-1.005, 0);
//			}
			
//			timer.schedule(dash, 100);
			dash();
			ataque1.reset();
		}
		
	};
	
	PersonalTimerTask ataque3 = new PersonalTimerTask() {
		
		public void run () {	
			caidaExplosiva = true;
			addForce(0, 6);
			setConstantForce(0, 1);
//			ataque3_active = false;
//			tp.delete();
			ataque3.reset();
		}
		
	};
	
	PersonalTimerTask waitForTp = new PersonalTimerTask() {
		
		public void run () {
			tp1.delete();
			tp2.delete();
			ataque3_2();
			waitForTp.reset();
		}
		
	};
	
	//disparar laser
	PersonalTimerTask ataque4_1 = new PersonalTimerTask() {
		
		public void run () {
			
			if (direccion.equals("R")) {
				laser = new Laser("laser", x1+50, y1+270, (x1+50)+1200, y1+357, 20, "rsc/graphic/projectil/laser_largo.gif", getField());
			} else {
				laser = new Laser("laser", (x2-50)-1200, y1+270, x2-50, y1+357, -20, "rsc/graphic/projectil/laser_largo.gif", getField());
				laser.flippedX = true;
			} 
			setVelocity(0, constantForce[1]);
			
			laser.orderInLayer = 80;
			lineaRojaLaser.delete();
			
			ataque4_1.reset();
		}
		
	};
	
	//borrar laser + linea roja
	PersonalTimerTask ataque4_2 = new PersonalTimerTask() {
		
		public void run () {
			laser.delete();
			ataque4_active = false;
			ataque4_2.reset();
		}
		
	};
	
	
	
	PersonalTimerTask dash = new PersonalTimerTask() {
		
		public void run () {
			setForce(0, 0);
			setVelocity(0, velocity[1]);
			
			if (direccion.equals("R")) {
				direccion = "L";
				flippedX = false;
			}
			
			if (direccion.equals("L")) {
				direccion = "R";
				flippedX = true;
			}
//			
			dash.reset();
		}
		
	};
	
	PersonalTimerTask robotMuerto = new PersonalTimerTask() {
		
		public void run () {
			delete();
			exploMuerte.delete();
			endBoom.delete();
			robotMuerto.reset();
		}
		
	};
	
	PersonalTimerTask boom = new PersonalTimerTask() {
		
		public void run () {
			endBoom = new Extra("", x1, y1, x2, y2, 0, "rsc/graphic/NPCs/enemies/boss robot/explosionFly.gif", getField());
			endBoom.orderInLayer = 70;
			boom.reset();
		}
		
	};
	
	PersonalTimerTask seVaLaNaveDeArriba = new PersonalTimerTask() {
		
		public void run () {
			topShip.topShipLeave = false;
			topShip.delete();
			w.showPopup("BOSS MUERTO, ABONO PA MI HUERTO");
			
			
			Evento portal = new Evento("portal", 1720, w.getSize().height-460, 1920, w.getSize().height-110, 0, "rsc/graphic/background/puerta.png", getField());
			Evento Q = new Evento("q", 1770, w.getSize().height-600, 1870, w.getSize().height-500, 0, "rsc/graphic/background/Q.png", getField());

//			portal.colorSprite = true;
//			portal.color = Color.yellow;
			
			seVaLaNaveDeArriba.reset();
		}
		
	};
	
	Window w;
	
	String direccion = "L";

	int position;

	int vida = 2000;
	
	boolean caidaExplosiva = false;
	
	Cohete p;
	
	/*FASES*/
	
	boolean fase1 = false;
	
	boolean fase2 = false;
	
	boolean fase3 = false;
	
	/*FLYMECHA*/
	
	Flymecha flymecha;
	
	boolean flymechaIN = false;
	
	/*EXTRA*/
	
	Extra topShip;
	
	Extra exploMuerte;
	
	Extra endBoom;
	
	/*BOMBA*/
	
	Bomba bomba;
	
	Extra lineaRojaLaser;
	
	Laser laser;
	
	/*EXTRA TP*/
	
	Extra tp;
	
	Extra tp1;
	
	Extra tp2;
	
	/*ATAQUES QUE SE ESTAN HACIENDO*/
	
	boolean ataque0_active = false;		//DISPARO
	
	boolean ataque1_active = false;		//SALTO + DASH
	
	boolean ataque2_active = false;		//COHETES
	
	boolean ataque3_active = false;		//TP + PISOTON + BARRIDO
	
	boolean ataque4_active = false;		//QUIETO + LASER
	
	//TRUE MIENTRAS HACE DASH HACIA LA DERECHA
	boolean dash_R = false;
	//TRUE MIENTRAS HACE DASH HACIA LA IZQUIERDA
	boolean dash_L = false;

	public Crabmecha(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, Window w) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0, 1);
		this.w = w;
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {

//		System.out.println("HA CHOCADO CON " + sprite.name);
		
		if (sprite.name.equals("Pared_R") || sprite.name.equals("Pared_L")) {
			this.dash_L = false;
			this.dash_R = false;
			this.ataque1_active = false;
			this.ataque3_active = false;
			
			if (sprite.name.equals("Pared_R")) {
				while(this.direccion.equals("R")) {
					this.direccion = "L";
					this.flippedX = false;
				}
			} else if (sprite.name.equals("Pared_L")) {
				while(this.direccion.equals("L")) {
					this.direccion = "R";
					this.flippedX = true;
				}
			}
			this.ataque0();
			this.ataque1();
		}
		
		if (sprite instanceof Jugador) {
			this.dash_R = false;
			this.dash_L = false;
			this.ataque1_active = false;
			this.ataque3_active = false;
			this.setVelocity(0, 0);
			this.setForce(0, 0);
			
			Jugador cyborg = (Jugador) sprite;

			cyborg.dash_L = false;
			cyborg.dash_R = false;
			
			cyborg.invulnerabilidad();
			cyborg.golpeado();
			cyborg.empuje(this);
			
//			System.out.println("ME HE CHOCADO CON EL JUGADORRRRRRRRRRRRRRRRR");
		}
		
		//ATAQUE 3
		if ( this.caidaExplosiva && ((sprite instanceof Objeto && sprite.name.equals("suelo")) || sprite instanceof Jugador) ) {
			//System.out.println("CRABMECHA HA CAIDO AL SUELO");
			if (sprite instanceof Objeto && sprite.name.equals("suelo")) {
				Bomba bombaL = new Bomba(this.bomba, this.x1-100, w.getSize().height-250, this.x1+100, w.getSize().height-145, 0);
				bombaL.derecha = false;
				
				Bomba bombaR = new Bomba(this.bomba, this.x2-100, w.getSize().height-250, this.x2+100, w.getSize().height-145, 0);
				bombaR.derecha = true;
			}
			
			this.ataque3_active = false;
			this.caidaExplosiva = false;
		}
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	
	//DISPARAR COHETE
	public void ataque0 () {
		
		this.ataque0_active = true;
		
		int mitat = this.y1+((this.y2 - this.y1)/2);
		
		if (this.direccion.equals("R")) {
			
			Cohete pium = new Cohete(this.p, this.x2, mitat-75, this.x2+100, mitat+75, 90, this.p.damage);
			
			pium.setVelocity(pium.vel, 0);
			
		} else {
			
			Cohete pium = new Cohete(this.p, this.x1-100, mitat-75, this.x1, mitat+75, -90, this.p.damage);
			
			pium.setVelocity(-pium.vel, 0);
			
		}
		
		this.ataque0_active = false;
		
	}
	
	//SALTO + EMBESTIDA
	public void ataque1() {
		
		this.ataque1_active = true;
		
		//SALTA
		this.setVelocity(0, 0);
		this.addForce(0, -6);
		
		timer.schedule(ataque1, 100);
		ataque1.reset();
		
	}
	
	//DISPARAR COHETES DEL CIELO
	public void ataque2(Flymecha flymecha) {
		
		this.ataque2_active = true;
		
		int num = r.nextInt(3)+1;
		
		switch(num) {
		case 1:
			this.generarBombas1(false);
			break;
		case 2:
			this.generarBombas2(false);
			break;
		case 3:
			this.generarBombas1(true);
			this.generarBombas2(true);
			break;
		}
		
		this.ataque2_active = false;
	}
	
	//SALTO + CA�DA EXPLOSIVA + BARRIDO
	public void ataque3() {
		
		this.ataque3_active = true;
		this.dash_L = false;
		this.dash_R = false;
		
		this.tp1 = new Extra("tpOUT", this.x1, this.y1, this.x2, this.y2, 0, "rsc/graphic/NPCs/enemies/boss robot/tpRobotOUT.gif", this.getField());
		this.tp2 = new Extra("tpIN", Main.cyborg.x1, 100, Main.cyborg.x1 + 200, 475, 0, "rsc/graphic/NPCs/enemies/boss robot/tpRobotIN.gif", this.getField());
		
		timer.schedule(waitForTp, 1000);
		
	}
	
	public void ataque3_2() {
		
		//TELEPORT
		this.x1 = Main.cyborg.x1;
		this.x2 = Main.cyborg.x1 + 200;
		this.y1 = 100;
		this.y2 = 475;
		this.setConstantForce(0, 0);
		this.setForce(0, 0);
//		this.tp = new Extra("tpIN", this.x1, this.y1, this.x2, this.y2, 0, "rsc/graphic/NPCs/enemies/boss robot/tpRobotOUT.gif", this.getField());
		
		timer.schedule(ataque3, 1000);

		ataque3.reset();
		
	}
	
	public void ataque4() {
		
		ataque4_active = true;
		
		if (Main.cyborg.x1 > this.x2) {
			this.direccion = "R";
			this.flippedX = true;
		} else {
			this.direccion = "L";
			this.flippedX = false;
		}
		
		
		//crear area roja
		if (this.direccion.equals("R")) {
			this.lineaRojaLaser = new Extra("laser", x1+50, y1+270, (x1+50)+1200, y1+357, 20, "", this.getField());
			this.setVelocity(3, this.constantForce[1]);
		} else {
			this.lineaRojaLaser = new Extra("laser", (x2-50)-1200, y1+270, x2-50, y1+357, -20, "", this.getField());
			this.setVelocity(-3, this.constantForce[1]);
		}
		
		this.lineaRojaLaser.colorSprite = true;
		this.lineaRojaLaser.color = new Color(255, 0, 0 ,50);
		this.lineaRojaLaser.orderInLayer = 80;
		
		timer.schedule(ataque4_1, 1000);
		timer.schedule(ataque4_2, 1500);
		
		ataque4_1.reset();
		ataque4_2.reset();
		
	}
	
	private void generarBombas1(boolean ignorar) {
		
		for (int i = 100;i <= 900;i+= 200) {
			
			if (ignorar && i == 900) {
				break;
			}
			
			Cohete pium = new Cohete(this.p, i, -150, i+100, 0, 180, this.p.damage);
			
			Extra roja = new Extra("linea roja 1", i, -150, i+100, this.w.getSize().height-170, 0, "", this.getField());
			roja.colorSprite = true;
			roja.color = new Color(255,0,0,50);
			
			pium.lineaRoja = roja;
			
			pium.addVelocity(0, 4);

			pium.animacion();
			
		}
		
		
		
		
	}
	
	private void generarBombas2(boolean ignorar) {

		for (int i = 900;i <= 1900;i+= 200) {
			
			if (ignorar && i == 900) {
				
			} else {
				Cohete pium = new Cohete(this.p, i, -150, i+100, 0, 180, this.p.damage);
				
				Extra roja = new Extra("linea roja 1", i, -150, i+100, this.w.getSize().height-170, 0, "", this.getField());
				roja.colorSprite = true;
				roja.color = new Color(255,0,0,50) ;
				
				pium.lineaRoja = roja;
				
				pium.addVelocity(0, 4);

				pium.animacion();
			}
			
			
			
		}
		
	}

	@Override
	public void danyar(int damage) {
		this.vida-= damage;
		if (this.vida <= 0 && this.vivo) {
			System.out.println("MUEREEEEEEEEEEEE");
			this.morir();
		}
		
	}
	
	@Override
	public void morir() {
		this.vivo = false;
		
		this.exploMuerte = new Extra("", this.x1, this.y1, this.x2, this.y2, 0, "rsc/graphic/projectil/boom.gif", this.getField());
		timer.schedule(robotMuerto, 3500);
		
		timer.schedule(boom, 2000);
		
		this.topShip.topShipLeave = true;
		timer.schedule(seVaLaNaveDeArriba, 5000);
	}
	
	public void dash() {
//		System.out.println("DASSSSSSSSSSSSSSSSSSSSSSSSSSHHHHHHHHHH");
		
		if (this.direccion.equals("R")) {
			this.position = this.x1;
			this.dash_R = true;
			
//			this.setVelocity(0, 0);
			this.addForce(1.5, 0);
		}
		
		if (this.direccion.equals("L")){
			this.position = this.x2;
			this.dash_L = true;
			
//			this.setVelocity(0, 0);
			this.addForce(-1.5, 0);
		}
		
	}

	public void update () {
		
		if (this.vivo) {
			
//			System.out.println("VIDA CRABMECHA: " + this.vida);
			
			this.time++;
			
			if (this.lineaRojaLaser != null) {
				if (this.direccion.equals("R")) {
					
					this.lineaRojaLaser.x1 = this.x1+50;
					this.lineaRojaLaser.y1 = this.y1+270;
					this.lineaRojaLaser.x2 = (this.x1+50)+1200;
					this.lineaRojaLaser.y2 = this.y1+357;
					this.lineaRojaLaser.angle = 20;
					
				} else {
					
					this.lineaRojaLaser.x1 = (this.x2-50)-1200;
					this.lineaRojaLaser.y1 = this.y1+270;
					this.lineaRojaLaser.x2 = this.x2-50;
					this.lineaRojaLaser.y2 = this.y1+357;
					this.lineaRojaLaser.angle = -20;
				}
				
				
			}
			
//			System.out.println("TIMEEEEEEE: " + this.time);
			//((this.time - ((this.time*33)/100)) % 50)
			
			//CAMINAR
//			if (!this.ataque1_active && !this.ataque3_active) {
//				if (this.direccion.equals("R")) {
//					this.setVelocity(5, this.constantForce[1]);
//				} else {
//					this.setVelocity(-5, this.constantForce[1]);
//				}
//			}
			
			if ( (this.time % 25) == 0) {
				
				this.segundos++;
				
//				int num = r.nextInt(2);
//				if (num == 1) {
//					this.direccion = "R";
//				} else {
//					this.direccion = "Laa";
//				}
				
//				System.out.println("-----------------------------");
//				System.out.println("ATAQUEEEEEEEEEEEEEEEEE");
//				//tiempo de vida del boss
//				System.out.println("SEGUNDOOOOOSSSS: " + this.segundos + " segundos");
//				System.out.println("-----------------------------");
			}
			
			if (this.vida > 1900) {
				if ( (this.time % 75) == 0 && (!this.ataque0_active && !this.ataque1_active)) {
					this.ataque0();
					this.ataque1();
					
				}
			} else if (this.vida > 1400) {		//ATAQUE DASH + DISPARO O TP PISOTON + BARRIDO
				
				if ( (this.time % 75) == 0) {
					int num = r.nextInt(3)+1;
					if ((num == 1 || num == 2) && !this.ataque3_active) {
						this.ataque0();
						this.ataque1();
					}
					
				}
				
				if ( (this.time % 125) == 0) {
					int num = r.nextInt(2)+1;
					if (num == 1 && (!this.ataque1_active)) {
						this.ataque3();
					}
					
				}
				
					
				
				
			} else if (this.vida > 900) {	//ATAQUE DASH + DISPARO I COHETES AEREOS
				
				if (!this.flymechaIN) {
					this.flymecha = new Flymecha("crabmecha", 700, -200, 1200, 0, 0, "rsc/graphic/NPCs/enemies/boss robot/FlyCrabmecha.gif", this.getField(), w);
					flymecha.orderInLayer = 30;
					this.flymechaIN = true;
					
					flymecha.bajar();
				}
				
				
				if ( (this.time % 75) == 0) {
					this.ataque0();
					this.ataque1();
				}
				
				if ( (this.time % 150) == 0 && this.flymecha.vivo) {
					ataque2(flymecha);
				}
			
				
			} else if (this.vida > 500) {		//ATAQUE DASH + DISPARO O TP PISOTON + BARRIDO
				
				if ( (this.time % 75) == 0 && !this.ataque3_active) {
					int num = r.nextInt(3)+1;
					if ((num == 1 || num == 2) ) {
						this.ataque0();
						this.ataque1();
					}
					
				}
				
				if ( (this.time % 125) == 0 && (!this.ataque1_active || !this.ataque0_active)) {
					int num = r.nextInt(2)+1;
					if (num == 1) {
						this.ataque3();
					}
					
				}
				
				if ( (this.time % 150) == 0 && this.flymecha.vivo) {
					ataque2(flymecha);
				}
				
				
			} else if (this.vida > 0) {
				
				if (this.flymechaIN && this.flymecha.vivo) {
					this.flymecha.subir();
					this.flymechaIN = false;
				}
				
				this.path = "rsc/graphic/NPCs/enemies/boss robot/DarkCrabmecha.gif";
				
				if (this.ataque4_active || this.ataque1_active || this.ataque3_active) {
					
				} else {
					if (this.time % 75 == 0) {
						int num = r.nextInt(3);
						if (num == 1 || num == 2) {
							this.ataque1();
						} else {
							this.ataque4();
						}
					}
					
					if ( (this.time % 125) == 0 && (!this.ataque4_active || !this.ataque1_active)) {
						int num = r.nextInt(2);
						if (num == 1) {
							this.ataque3();
						}
						
					}
				}
				
			}
			
			
			
		}
		
		if (this.vida <= 1000) {
			this.fase1 = true;
		}
		if (this.vida <= 700) {
			this.fase2 = true;
		}
		if (!this.vivo) {
			this.fase3 = true;
		}
		
		if (this.y2 < 0) {
			if (!this.ataque3_active) {
				this.ataque3();
			}
		}
		
		if (this.dash_R) {
			if ((this.x1 - this.position) >= 600) {
				this.setForce(0, 0);
				this.setVelocity(0, this.velocity[1]);
				this.dash_R = false;
				this.dash_L = false;
				
				int num = r.nextInt(2)+1;
				
				if (num == 1) {
					this.direccion = "L";
					this.flippedX = false;
				} else {
					this.direccion = "R";
					this.flippedX = true;
				}
				
				this.ataque1_active = false;
				
				
			}
		}
		
		if (this.dash_L) {
			if ((this.position - this.x2) >= 600) {
				this.setForce(0, 0);
				this.setVelocity(0, this.velocity[1]);
				this.dash_L = false;
				this.dash_R = false;
				
				int num = r.nextInt(2)+1;
				
				if (num == 1) {
					this.direccion = "R";
					this.flippedX = true;
				} else {
					this.direccion = "L";
					this.flippedX = false;
				}
				
				this.ataque1_active = false;
				
			}
		}
		
		
	}

	
	

}
