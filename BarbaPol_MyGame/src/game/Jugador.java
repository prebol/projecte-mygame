package game;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
//FILES
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

//ALTRES
import java.util.ArrayList;
import java.util.Iterator;
//TIMER
import java.util.Timer;
import java.util.TimerTask;

//CORE
import Core.Field;
import Core.Observer;
import Core.PhysicBody;
import Core.Sprite;
import Core.Window;

public class Jugador extends PhysicBody implements Observer {
	
	//HE COPIADO TIMER & TIMERTASK PARA PODER MODIFICARLOS
	PersonalTimer timer = new PersonalTimer();
	
	//AQUEST TIMER SERVEIX PER DISPARAR AMB ESPAI ENTRE BALA I BALA
	PersonalTimerTask dispararTrue = new PersonalTimerTask() {
		
		public void run () {
			disparar = true;
			dispararTrue.reset();
		}
		
	};

	//AQUEST TIMER SERVEIX PER DASHEAR AMB ESPAI ENTRE DASH I DASH
		PersonalTimerTask dashTrue = new PersonalTimerTask() {
			
			public void run () {
				dash = true;
				dashTrue.reset();
			}
			
		};
	
	PersonalTimerTask task_Invulnerable = new PersonalTimerTask() {
		
		public void run () {
			invulnerable = false;
//			path = idle[1];
			mover = true;
			task_Invulnerable.reset();
		}
		
	};
	
	PersonalTimerTask task_Hurt = new PersonalTimerTask() {
		
		public void run () {
			task_Hurt.reset();
		}
		
	};
	
	PersonalTimerTask stopForce = new PersonalTimerTask() {
		
		public void run () {
			setForce(0, 0);
			stopForce.reset();
		}
		
	};
	
	PersonalTimerTask morir = new PersonalTimerTask() {
		
		public void run () {
			visible = false;
			w.showPopup("HAS MUERTO");
			Main.currentMapId = 1;
			Main.changeMap();
			morir.reset();
		}
		
	};
	
	//JUGADOR
	
	double spd = 12.5;
	
	double jumpforce = 6;
	
	boolean aterra = false;
	
	boolean quieto = false;
	
	boolean vivo = true;
	
	boolean invulnerable = false;
	
	boolean mover = true;
	
	boolean arriba = false;
	
	int iniX1 = 100;
	
	//int iniY1 = 100;
	int iniY1 = Main.w.getSize().height-275;
		
	int iniX2 = 160;
	
//	int iniY2 = 205;
	int iniY2 = Main.w.getSize().height-170;
	
	/*IMAGES*/
	
	String [] idle = new String[] {"rsc/graphic/jugador/player_idle_L.gif",	//LEFT
								"rsc/graphic/jugador/player_idle_R.gif"};	//RIGHT
	
	String [] run = new String[] {"rsc/graphic/jugador/player_running_L.gif",	//LEFT
								"rsc/graphic/jugador/player_running_R.gif"};	//RIGHT
	
	String [] jump = new String[] {"rsc/graphic/jugador/player_jump_L.gif",	//LEFT
								"rsc/graphic/jugador/player_jump_R.gif"};	//RIGHT
	
	String [] pain = new String[] {"",	//LEFT
								"rsc/graphic/jugador/player_hurt_R2.gif"	//RIGHT
									};
	
	String [] death = new String[] {"",	//LEFT
								"rsc/graphic/jugador/player_death_R.gif"	//RIGHT
									};
	
	//DASH
	
	boolean dash = true;
	
	int dashforce = 15;
	
	int dashdistance = 200;
	
	boolean dash_R = false;
	
	boolean dash_L = false;
	
	int position;
	
	//PROYECTIL
	
	double bulletVel = 20;
	
	String direccion;
	
	boolean disparar = true;
	
	//PARA DASH PATH
	String lastpath;
	
	//PROYECTIL QUE DISPARA EL JUGADOR
	Projectil p;
	
	private Field f;
	
	//WINDOW
	private Window w;

	
	public Jugador(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, Window w) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0, 1);
		this.direccion = "R";
		this.w = w;
		this.f = f;
		this.collisionBox = true;
		this.w.subscribe(this);
		this.f.subscribe(this);
	}


	public void onCollisionEnter(Sprite sprite) {
		
		if (sprite instanceof Objeto && sprite.name.equals("suelo")) {
			aterra = true;
		}
		
		if (sprite.name.equals("Pared")) {
			this.dash_L = false;
			this.dash_R = false;
		}
		
		
//		if (sprite instanceof Projectil && ((Projectil) sprite).enemigo) {
//			sprite.delete();
//			UI.getInstance().vidas.vidas--;
//			
//			System.out.println("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
//			
//			if (UI.getInstance().vidas.vidas == 0) {
//				this.morir();
//			}
//		}
		
//		System.out.println("El jugador ha chocado con: \n"+sprite);
		
	}

	public void onCollisionExit(Sprite sprite) {
		
	}
	
	public void onTriggerEnter(Sprite sprite) {
		
		if (sprite instanceof Bomba || sprite instanceof Laser) {
			this.invulnerabilidad();
			this.golpeado();
			
		}
	}


	public void moviment(Input moviment) {
		
		if (this.dash_R || this.dash_L) {
			
		} else {
			if (moviment == Input.ESQUERRA) {
				
				if (!this.invulnerable) {
					this.drawingBoxExtraRight = 0;
					this.drawingBoxExtraLeft = 40;
					
					if (this.aterra) {
						this.path = run[0];
					} else {
						this.path = jump[0];
					}
				}
				
				this.setVelocity(-this.spd, this.velocity[1]);
				this.quieto = false;
				this.direccion = "L";
				
			}
			
			if (moviment == Input.DRETA) {
				
				if (!this.invulnerable) {
					this.drawingBoxExtraRight = 40;
					this.drawingBoxExtraLeft = 0;
					
					if (this.aterra) {
						this.path = run[1];
						
					} else {
						this.path = jump[1];
					}
				}
				
				this.setVelocity(this.spd, this.velocity[1]);
				this.quieto = false;
				this.direccion = "R";
				
//				System.out.println("DRETA MOVIMENT");
				
			}
			
		}
		
	}
	
	public void saltar(Input saltar) {
		if (this.dash_R || this.dash_L) {
				
		} else {
			if (this.aterra) {
				this.addForce(0, -jumpforce);
				this.aterra = false;
				if (this.quieto) {
					
				} else {
					if (!this.invulnerable) {
						if (this.direccion.equals("R")) {
							this.path = jump[1];
						} 
						if (this.direccion.equals("L")) {
							this.path = jump[0];
						}
					}
				}
			}
		}
	}


	public void quieto() {
		
		if (this.dash_R || this.dash_L) {
			
		} else {
			this.setVelocity(0, this.velocity[1]);
			this.quieto = true;
			
			if (!this.invulnerable) {
				if (this.direccion.equals("R")) {
					this.path = idle[1];
				} else {
					this.path = idle[0];
				}
				
				this.drawingBoxExtraLeft = 0;
				this.drawingBoxExtraRight = 0;
			}
			
			
				
//			if (this.aterra) {
//				if (this.direccion.equals("R")) {
//					this.path = "rsc/graphic/jugador/player_idle_R.gif";
//				} else {
//					this.path = "rsc/graphic/jugador/player_idle_L.gif";
//				}
//				
//				this.x2 = this.x1 + 60;
//			}
		}
		
		
	}
	
	public void dash(Input dash) {
		
		if ((!this.dash_R && !this.dash_L) && this.dash) {
			
			if (this.direccion.equals("R")) {
				this.position = this.x1;
				this.dash_R = true;
				
				
//				this.lastpath = this.path;
//				this.path = "rsc/jugador/player_dash_R.gif";
				
				
				this.setVelocity(0, 0);
				this.setForce(this.dashforce, 0);
				
				this.drawingBoxExtraRight = 40;
				
			}
			
			if (this.direccion.equals("L")){
				this.position = this.x2;
				this.dash_L = true;
				
				this.setVelocity(0, 0);
				this.setForce(-this.dashforce, 0);
				
				this.drawingBoxExtraLeft = 40;
			}
			
			
			this.dash = false;

			timer.schedule(dashTrue, 700);
			
		}
		
		//HACE TP EN VEZ DE DASH
		/*if (this.direccion.equals("R")) {
		
			this.x1 += 200;
			this.x2 += 200;
		
		}
		
		if (this.direccion.equals("L")){
	
			this.x1 -= 200;
			this.x2 -= 200;
		}*/
		
		
	}


	public void disparar(Input dispara) {
		
		if (this.disparar && this.p != null) {
			
			int mitat = this.y1+((this.y2 - this.y1)/2);
			
			int mitatY = this.x1+((this.x2 - this.x1)/2);
			
			double velDiagonal = Math.sqrt(this.bulletVel * this.bulletVel) + (this.bulletVel * this.bulletVel);
			
			
			if (this.quieto) {
				
				if (this.arriba) {
					
					Projectil pium = new Projectil(this.p, mitatY-30, this.y1-35, mitatY+30, this.y1, 90, this.p.damage);
					
					pium.setVelocity(0, -bulletVel);
					
				} else {
					
					if (this.direccion.equals("R")) {
						
						Projectil pium = new Projectil(this.p, this.x2, mitat-20, this.x2+55, mitat+20, 180, this.p.damage);
						
						pium.setVelocity(bulletVel, 0);
						
					} else {
						
						Projectil pium = new Projectil(this.p, this.x1-55, mitat-20, this.x1, mitat+20, 0, this.p.damage);
						
						pium.setVelocity(-bulletVel, 0);
					}
				}
				 
			} else {
				
				if (this.direccion.equals("R")) {
					
					if (this.arriba) {
						
						Projectil pium = new Projectil(this.p, mitatY-30, this.y1-35, mitatY+30, this.y1, 90, this.p.damage);
						
						pium.setVelocity(0, -bulletVel);
						
//						Projectil pium = new Projectil(this.p, mitatY-30, this.y1-35, mitatY+30, this.y1, 135, this.p.damage);
//						
//						pium.setVelocity(bulletVel/2, -bulletVel/2);
						
					} else {
						
						Projectil pium = new Projectil(this.p, this.x2, mitat-20, this.x2+55, mitat+20, 180, this.p.damage);
						
						pium.setVelocity(bulletVel, 0);
						
					}
					
					
					
				} else if (this.direccion.equals("L")) {
					
						if (this.arriba) {
							
							Projectil pium = new Projectil(this.p, mitatY-30, this.y1-35, mitatY+30, this.y1, 90, this.p.damage);
							
							pium.setVelocity(0, -bulletVel);
						
//						Projectil pium = new Projectil(this.p, mitatY-30, this.y1-35, mitatY+30, this.y1, 45, this.p.damage);
//						
//						pium.setVelocity(-bulletVel/2, -bulletVel/2);
						
						} else {
							
							Projectil pium = new Projectil(this.p, this.x1-55, mitat-20, this.x1, mitat+20, 0, this.p.damage);
							
							pium.setVelocity(-bulletVel, 0);
							
						}
					
				}
				
//					if (this.arriba) {
//						Projectil pium = new Projectil(this.p, mitatY-30, this.y1-55, mitatY+30, this.y1, 90, this.p.damage);
//						pium.setVelocity(-bulletVel, -bulletVel);
//					} else {
//						Projectil pium = new Projectil(this.p, this.x1-55, mitat-30, this.x1, mitat+30, 0, this.p.damage);
//						
//						pium.setVelocity(-bulletVel, 0);
//					}
			}
			
			this.disparar = false;
			
			w.playSFX("rsc/sound/sfx/Jlaser.wav");
			
			timer.schedule(dispararTrue, 300);
		
		}	
	}
	
	public void empuje(Boss boss) {

		/*
		if (this.direccion.equals("R")) {
			this.addForce(-5, -2);
		} else if (this.direccion.equals("L")) {
			this.addForce(5, -2);
		}
		
		*/
		
		if (this.x1 > boss.x2) {
			this.addForce(+5, -2);
		}
		if (boss.x1 > this.x2) {
			this.addForce(-5, -2);
		}
		
		timer.schedule(stopForce, 700);
		
	}
	
	public void morir() {
		this.vivo = false;

		this.path = death[1];
		
		if (this.direccion.equals("L")) {
			this.flippedX = true;
		}
		
		timer.schedule(morir, 500);
	}
	
	public void invulnerabilidad() {

//		if(this.invulnerable)
//			return;
		
		this.invulnerable = true;
		this.mover = false;
		this.path = "rsc/graphic/jugador/player_hurt_R2.gif";
		this.x2 = this.x1 + 60;
		this.y2 = this.y1 + 105;
		timer.schedule(task_Invulnerable, 1500);
		task_Invulnerable.reset();
		
	}
	
	public void golpeado () {
		
		UI.getInstance().vidas.vidas--;
//		this.path = this.pain[1];
//		timer.schedule(task_Hurt, 1000);
		
		
		if (UI.getInstance().vidas.vidas == 0) {
			this.morir();
		}
		
	}
	
	public void tpPlayer () {
		this.x1 = 100;
		this.x2 = 160;
		this.y1 = 100;
		this.y2= 205;
		this.setVelocity(0, this.constantForce[1]);
		this.setForce(0, 0);
		this.dash_L = false;
		this.dash_R = false;
		this.path = idle[1];
	}
	
	public void update () {
		
//		System.out.println("DISPARAR: " + this.disparar);
//		System.out.println("aterra:" + this.aterra);
		
//		System.out.println("-------------------");
//		System.out.println("X1: " + this.x1);
//		System.out.println("Y1: " + this.y1);
//		System.out.println("X2: " + this.x2);
//		System.out.println("Y2: " + this.y2);
//		System.out.println("velocitat personatge: [" + this.velocity[0] + ", " + this.velocity[1] + "]");
//		System.out.println("-------------------");
		
		if(this.dash_R || this.dash_L) {
//			System.out.println("-------------------");
//			System.out.println("velocitat personatge: [" + this.velocity[0] + ", " + this.velocity[1] + "]");
//			System.out.println("for�a personatge: [" + this.force[0] + ", " + this.force[1] + "]");
//			System.out.println("-------------------");
			
		}
		
		if (this.dash_R) {
			if ((this.x1 - this.position) >= 200) {
				this.setForce(0, 0);
				this.setVelocity(0, this.velocity[1]);
				this.dash_R = false;
				this.dash_L = false;
				//this.path = this.lastpath;
				
				this.drawingBoxExtraRight = 0;
				
//				System.out.println("--------R-----------");
//				System.out.println("posicio inicial dash (x): " + this.position);
//				System.out.println("posicio final dash (x): " + this.x2);
//				System.out.println("-------------------");
				
				
			}
		}
		
		if (this.dash_L) {
			if ((this.position - this.x2) >= 200) {
				this.setForce(0, 0);
				this.setVelocity(0, this.velocity[1]);
				this.dash_L = false;
				this.dash_R = false;
				
				this.drawingBoxExtraRight = 0;
				
//				System.out.println("---------L----------");
//				System.out.println("posicio al fer dash (x): " + this.position);
//				System.out.println("posicio final dash (x): " + this.x1);
//				System.out.println("-------------------");
				
				
			}
		}
		
		
		if (this.velocity[0] >= 120 || this.velocity[0] <= -120) {
			this.setForce(0, 0);
		}
		
//		System.out.println("dash R: " + this.dash_R);
//		System.out.println("dash L: " + this.dash_L);
//		
//		System.out.println(this.getW().getSize().height);
//		System.out.println(this.getW().getSize().width);
		
//		System.out.println(f.getSprites());
		
//		
//		System.out.println("/////////");
//		System.out.println("cont: " + this.cont);
//		System.out.println("/////////");
//		
		/*
		if (this.dash_R || this.dash_L) {
			
			this.cont++;
			
			if (this.cont == 6) {
				this.setForce(0, 0);
				this.dash_R = false;
				this.dash_L = false;
				this.cont = 0;
				this.booleano=false;
			}
		}
		*/
		
		//PARA NO SALIRSE DE LA PANTALLA CON LOS TP's
		
		/*
		if (this.x1 <= 0) {
			this.x1 += 15;
			this.x2 += 15;
		}
		
		if (this.x2 >= w.getSize().width-25) {
			this.x2 -= 25;
			this.x1 -= 25;
		}
		*/
	}

	public Field getF() {
		return f;
	}


	public Window getW() {
		return w;
	}

	public void spawnearBoss() {
		
		int altura = getW().getSize().height;
		int base = getW().getSize().width;
		
		GiagantEye mamabicho = new GiagantEye("Enemigo 1", base-200, altura-700, base, altura-400, 0, "rsc/graphic/NPCs/enemies/boss_ojo/ojo_boss.gif", Main.fs[0]);
		
		Projectil bomb = new Projectil("BOMB Parabola", 0, 0, 0, 0, 0, "rsc/graphic/projectil/rasengan.gif", Main.fs[0], getW());
		
		mamabicho.p = bomb;
		
	}


	@Override
	public void keyPressed(KeyEvent e) {
//		System.out.println("vivo: " + this.equadddddddd
		char caracter = e.getKeyChar();
		int codi = e.getKeyCode();
		String nomTecla = e.getKeyText(codi);

//		System.out.println("nom tecla: " + nomTecla);
		if (nomTecla.equals("Right")) {
			moviment(Input.DRETA);
//			System.out.println("MOVIENDO A LA DERECHA CON LA FLECHA DERECHA");
		}
		
	}


	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	

}
