package game;

import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.Sprite;

public class GiagantEye extends Boss {
	
	Timer timer = new Timer();
	
	TimerTask atacar = new TimerTask() {
		
		public void run () {
			disparar();
		}
	};
	
	/*-----PROJECTIL-----*/
	
	double bulletVel = 20;
	
	Projectil p;
	
	public GiagantEye(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
//		timer.schedule(atacar, 0, 1000);
		this.setConstantForce(0, 0);
		this.vivo = true;
	}

	
	
	public void disparar() {
		
		if (this.vivo) {
			int mitat = this.y1+((this.y2 - this.y1)/2);
			
			Projectil pium = new Projectil(this.p, this.x1-55, mitat-30, this.x1, mitat+30, 0, 20);
			
			pium.enemigo = true;
			
			pium.setVelocity(-bulletVel, 0);
			
			Main.w.playSFX("rsc/sound/sfx/OjoLaser.wav");
		}
		
		
	}
	
	public void update () {
		
		//ARRIBA
		if (this.sentido.equals("T")) {
			this.y1-= 2;
			this.y2-= 2;
			this.pos-= 2;
			if (this.pos <= -200) {
				this.sentido = "B";
			}
		}
		
		//ABAJO
		if (this.sentido.equals("B")) {
			this.y1+= 2;
			this.y2+= 2;
			this.pos+= 2;
			if (this.pos >= 200) {
				this.sentido = "T";
			}
		}
		
//		System.out.println("VIDAAAAAAAAAA: " + this.vida);
		
	}


	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void morir() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void danyar(int damage) {
		// TODO Auto-generated method stub
		
	}
	
}
