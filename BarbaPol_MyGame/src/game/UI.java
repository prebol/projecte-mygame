package game;

import Core.Field;

public class UI {
	
    public static UI instance = null;
    
    public Vidas vidas;
    
    public Mouse mouse;
    
    public VidaBoss vidaBoss;
    
    //en un singleton el Constructor es privat
    private UI() {
//    	this.vidas = new Vidas("vidas", 15, 50, 300, 90, 0, "", Main.fs[0]);
    	/*BOSS 1*/
    	this.vidas = new Vidas("vidas", 15, Main.w.getSize().height-120, 300, Main.w.getSize().height-80, 0, "", Main.fs[0]);

    	/*BOSS 2*/
    	this.vidas = new Vidas("vidas", 15, Main.w.getSize().height-140, 300, Main.w.getSize().height-100, 0, "", Main.fs[3]);
    	this.vidas.orderInLayer = 100;
    	
//    	this.vidaBoss = new VidaBoss("vidaBoss", 400, Main.w.getSize().height-140, 800, Main.w.getSize().height-100, 0, "", Main.fs[3]);
//    	this.vidaBoss.orderInLayer = 100;

    	//    	this.mouse = new Mouse("mouse", 0, 0, 25, 25, 0, "", Main.fs[2]);
    }
    
    public static UI getInstance() {
    	
        //si no existeix el creem
        if(instance == null) {
            instance = new UI();
        }
        //tant si ja existia com si l'hem creat, tornem instance
        return instance;
    }
}
