package game;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Core.Window;

public class Projectil extends PhysicBody {
	
	PersonalTimer timer = new PersonalTimer();
	
	
	PersonalTimerTask boom = new PersonalTimerTask() {
		
		public void run () {
			delete();
			boom.reset();
		}
		
	};
	
	Window w;
	
	//DA�O QUE HACE EL PROYECTIL
	int damage;
	
	boolean enemigo = false;
	
	Extra lineaRoja;

	public Projectil(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, Window w) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.trigger = true;
		this.w = w;
	}
	
	public Projectil(Projectil p, int x1, int y1, int x2, int y2, double angle, int damage) {
		super(p.name, x1, y1, x2, y2, angle, p.path, p.f);
		this.trigger = true;
		this.w = p.w;
		this.damage = damage;
		this.enemigo = p.enemigo;
		this.orderInLayer = p.orderInLayer;
		this.colorSprite = p.colorSprite;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {

		//BOSS
		if (sprite instanceof Disparable && !this.enemigo) {
			this.explotar();
			((Boss) sprite).danyar(this.damage);
//			UI.getInstance().vidaBoss.x2-= 5;

//			System.out.println("DAMAGEEEEEEEEEEEEEEEEEEEEE: " + this.damage);
		} else {
			if (sprite instanceof Jugador && this.enemigo) {
				this.explotar();
				Jugador j = (Jugador) sprite;
				if (!j.invulnerable) {
					j.golpeado();
					j.invulnerabilidad();
				} else {
					System.out.println("el jugador es invulnerable");
				}
			}
		}
		
		if (sprite instanceof Mouse) {
			this.delete();
		}
		
		if (sprite instanceof Objeto) {
			this.explotar();
		}
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onTriggerEnter(Sprite sprite) {

		if (sprite instanceof Disparable && !this.enemigo) {
			this.explotar();
			((Boss) sprite).danyar(this.damage);

//			System.out.println("DAMAGEEEEEEEEEEEEEEEEEEEEE: " + this.damage);
		}
		
//		if (sprite instanceof Cohete) {
//			Cohete cohete = (Cohete) sprite;
//			
//			cohete.explotar();
//			this.explotar();
//		}
		
	}
	
	public void explotar() {

		if (this instanceof Cohete) {
			this.path = "rsc/graphic/projectil/exploCohete.gif";
		} else {
			this.path = "rsc/graphic/projectil/humo.gif";
		}
		this.setVelocity(0, 0);
		this.angle = 0;
		timer.schedule(boom, 500);
		boom.reset();
		if (this.lineaRoja != null) {
			this.lineaRoja.delete();
		}
		
	}
	
	public void update() {
		//System.out.println(this.x2);
		if (this.x2 < 0 || this.x1 > w.getSize().width) {
			this.delete();
		}
	}

}
