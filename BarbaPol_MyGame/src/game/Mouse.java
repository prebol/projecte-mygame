package game;

import Core.Field;
import Core.Sprite;

public class Mouse extends Sprite {
	
	private Field f;

	public Mouse(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.f = f;
		this.solid = false;
		this.path = "rsc/graphic/navi.gif";
	}

	public Field getF() {
		return f;
	}
	
	public void update() {
		
		this.x1 = getF().getMouseOverX();
		this.y1 = getF().getMouseOverY();
		
		this.x2 = getF().getMouseOverX()+50;
		this.y2 = getF().getMouseOverY()+50;
		
		System.out.println("-------------------");
		System.out.println(this.getF().getMouseOverX());
		System.out.println(this.getF().getMouseOverY());
		System.out.println("-------------------");
		
	}

}
