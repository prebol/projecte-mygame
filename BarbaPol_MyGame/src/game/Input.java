package game;

public enum Input {
	
	ESQUERRA,
	DRETA,
	SALTAR,
	AGACHAR,
	DISPARA,
	DASH;
}
