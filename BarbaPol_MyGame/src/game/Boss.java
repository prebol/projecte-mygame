package game;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public abstract class Boss extends PhysicBody implements Disparable {
	
	/*-----ENEMIGO-----*/
	
	boolean vivo;
	
	int vida;
	
	int vel;
	
	String direccion = "L";
	
//	int posIni = y2 - ((y2 - y1)/2);
	
	int pos = 0;
	
	String sentido = "T";
	
	/*TIEMPO*/
	
	int time = 0;
	
	int segundos = 0;

	PersonalTimer timer = new PersonalTimer();
	
	public Boss(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.vivo = true;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		
	};

	@Override
	public void onCollisionExit(Sprite sprite) {
		
	}

	@Override
	public abstract void morir();
	
	@Override
	public abstract void danyar(int damage);
	

}
