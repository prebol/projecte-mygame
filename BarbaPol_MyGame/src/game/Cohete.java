package game;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Cohete extends Projectil {
	
	PersonalTimerTask coheteFase1 = new PersonalTimerTask() {
		
		public void run () {
			setVelocity(0, 0);
			timer.schedule(coheteFase2, 400);
			coheteFase1.reset();
		}
		
	};
	
	PersonalTimerTask coheteFase2 = new PersonalTimerTask() {
		
		public void run () {
			setVelocity(0, velCaida);
			coheteFase2.reset();
		}
		
	};
	
	int velCaida = 25;
	
	int vel = 25;

	public Cohete(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, Window w) {
		super(name, x1, y1, x2, y2, angle, path, f, w);
	}
	
	public Cohete(Projectil p, int x1, int y1, int x2, int y2, double angle, int damage) {
		super(p, x1, y1, x2, y2, angle, damage);
	}
	
	@Override
	public void onTriggerEnter(Sprite sprite) {
		
//		if (sprite instanceof Projectil && (sprite.name.equals(Main.cyborg.p.name))) {
//			this.explotar();
//			((Projectil) sprite).explotar();
//		}
	}

	public void animacion() {

		timer.schedule(coheteFase1, 400);
		
	}
	

	

}
