package game;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Extra extends PhysicBody {
	
	boolean topShipLeave = false;
	
	int topShipVel = 30;

	public Extra(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.solid = false;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	
	public void update() {
		
		if (this.name.equals("tpIN")) {
			this.x1 = Main.cyborg.x1;
			this.x2 = Main.cyborg.x1 + 200;
		}
		
		if (topShipLeave) {
			this.setVelocity(0, topShipVel);
		}
		
	}

	

	

}
