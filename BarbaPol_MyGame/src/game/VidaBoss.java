package game;

import Core.Field;
import Core.Sprite;

import java.awt.Color;

public class VidaBoss extends Sprite {
	
//	int vida = 2000;

	public VidaBoss(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.solid = false;
		this.colorSprite = true;
		this.color = Color.green;
//		this.text = true;
//		this.path = "BOSS: ";
//		this.textColor = 255;
	}

	public void update () {
		
		if (((this.x2 - this.x1)*5) > 1400) {
			this.color = Color.green;
		} else if (((this.x2 - this.x1)*5) > 500) {
			this.color = Color.yellow;
		} else {
			this.color = Color.red;
		}
		
	}

}
