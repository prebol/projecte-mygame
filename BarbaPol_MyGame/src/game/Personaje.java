package game;

import java.util.ArrayList;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Core.Window;

public class Personaje extends PhysicBody {
	
	Field f;
	
	Window w;
	
	/*PERSONAJE*/
	
	int velocidad = 18;
	
	boolean mover;
	
	//1106, -657, 1181, -517
	
	int iniX1 = 1106;
	
	int iniY1 = -657;
	
	int iniX2 = 1181;
	
	int iniY2 = -517;
	
	/*POS MOUSE*/
	
	int posMX;
	
	int posMY;
	
	String direccion = "R";
	
	boolean chocando = false;
	
	/*PANTALLA
	 * Cada 1 pantalla es la resolucio de la pantalla en l'eix corresponent
	 * 
	 * */
	
	
	int pantX = 0;
	
	int pantY = 0;
	
	/*/////////*/
	boolean scroll = false;
	/*/////////*/
	
	/*IMAGES*/
	
	String [] idle = new String[] {"rsc/graphic/jugador/player_idle_L.gif",	//LEFT
								"rsc/graphic/jugador/player_idle_R.gif"};	//RIGHT
	
	String [] run = new String[] {"rsc/graphic/jugador/player_running_L.gif",	//LEFT
								"rsc/graphic/jugador/player_running_R.gif"};	//RIGHT


	public Personaje(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, Window w) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.solid = true;
		this.f = f;
		this.w = w;
		this.mover = false;
		this.setConstantForce(0, 0);
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {

		if (sprite instanceof Objeto) {
			chocando = true;
		}
		
		System.out.println("PERSONAJE HA CHOCADO CON ALGO");
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {

		if (sprite instanceof Objeto) {
			chocando = false;
		}
		
		System.out.println("PERSONAJE HA PARADO DE CHOCAR CON ALGO");
		
	}
	
	@Override
	public void onTriggerEnter(Sprite sprite) {
		
		System.out.println("PERSONAJE HA CHOCADO CON ALGUN TRIGGER");
		
	}
	
	public void moviment (InputP input) {
		
		if (!this.mover && !this.chocando) {
			if (input == InputP.TOP) {
				
				this.y1-= this.velocidad;
				this.y2-= this.velocidad;
				
				if (this.direccion.equals("R")) {
					this.path = run[1];
					this.drawingBoxExtraRight = 45;
					this.drawingBoxExtraLeft = 0;
				} else {
					this.path = run[0];
					this.drawingBoxExtraRight = 0;
					this.drawingBoxExtraLeft = 45;
				}
				
				
			}
			
			if (input == InputP.BOTTOM) {
				
				this.y1+= this.velocidad;
				this.y2+= this.velocidad;
				
				if (this.direccion.equals("R")) {
					this.path = run[1];
					this.drawingBoxExtraRight = 45;
					this.drawingBoxExtraLeft = 0;
				} else {
					this.path = run[0];
					this.drawingBoxExtraRight = 0;
					this.drawingBoxExtraLeft = 45;
				}
			}
			
			if (input == InputP.RIGHT) {
				
				this.drawingBoxExtraRight = 45;
				this.drawingBoxExtraLeft = 0;
				
				this.x1+= this.velocidad;
				this.x2+= this.velocidad;
					
				this.path = run[1];
				
				this.direccion = "R";
			}
			
			if (input == InputP.LEFT) {
				
				this.drawingBoxExtraRight = 0;
				this.drawingBoxExtraLeft = 45;
				
				this.x1-= this.velocidad;
				this.x2-= this.velocidad;
				
				this.path = run[0];

				
				this.direccion = "L";
				
			}
			
		}
		
		
		
		
				
	}
	
	public void quieto() {
		
		this.drawingBoxExtraLeft = 0;
		this.drawingBoxExtraRight = 0;
		
		if (this.direccion.equals("R")) {
			this.path = idle[1];
		} else if (this.direccion.equals("L")) {
			this.path = idle[0];
		}
		
	}
	
	public void andar() {
		
		this.mover = true;
		
	}
	
	public void update () {
//		System.out.println(this.getField().getSprites());
		if (this.mover) {
//			System.out.println("mover es TRUEEEEEEEEEEEEEEEE");
			System.out.println(Main.fs[1].getCurrentMouseSpriteP());
			ArrayList<Sprite> listaSpritesClickados = this.getField().getCurrentMouseSpriteP();
			for(Sprite s : listaSpritesClickados) {
//				Caja nueva = (Caja) s;
//				System.out.println("IN THE SPRITEEEEEEEEEEEEEEE");
//				System.out.println("NOM SPRITE: " + s.name);
				switch(s.name) {
				case "luchar":
//					currentMapId = 3;
//					a�adirFieldCyborg();
//					generarBossRobot();
//					changeMap();
					System.out.println("LUCHAAAAAAAAARRRRRRRRRRRRRRRRRR");
					break;
				case "salir":
//					System.out.println("nueva");
//					nueva.habilitat = false;
//					Caja hola = new Caja("mensaje", (w.getSize().width/2)-350, (w.getSize().height/2)-300, (w.getSize().width/2)+350, (w.getSize().height/2)+300, 0, "rsc/graphic/menu/panel.png", fs[2]);
////						for (Sprite sCaja: fs[2].getSprites()) {
////							sCaja.visible = false;
////						}
//					newGame();
					System.out.println("SALIIIIIIIIIIIIIIIIIIIIRRRRRRRR");
					break;
				default:
//					System.out.println("NO HA CLICAT CAPPPPPPPPPPPPPPPPPP");
					break;
				}
				
				
			}
		}
		
//		System.out.println("------------------------------------");
//		System.out.println("getCurrentMouseX: " + this.getField().getCurrentMouseX());
//		System.out.println("getCurrentMouseY: " + this.getField().getCurrentMouseY());
//		System.out.println("posMX: " + this.getField().getMouseX());
//		System.out.println("posMY: " + this.getField().getMouseY());
		
//		System.out.println("mouseX: " + this.posMX*(pantX+1));
//		System.out.println("mouseY: " + this.posMY*(pantY+1));
//		
//		System.out.println("x2: " + this.x2);
//		System.out.println("y2: " + this.y2);
		
//		if (this.mover) {
//			
//			int ubiX = posMX + (pantX * w.getSize().width);
//			
//			int ubiY = posMY + (pantY * w.getSize().height);
//			
//			if ((this.x2 >= ((this.posMX - 15) + (pantX * w.getSize().width)) && this.x2 <= ((this.posMX +15) + (pantX * w.getSize().width))) && (this.y2 >= ((this.posMY-15) + (pantX * w.getSize().height)) && this.y2 <= ((this.posMY+15) + (pantX * w.getSize().height)))) {
//				System.out.println("patata");
////				f.setMouseX(-1);
////				f.setMouseY(-1);
//				this.mover = false;
//				this.quieto();
//				
//			} else {
//				if (ubiX < this.x2) {
//					this.x1-= this.velocidad;
//					this.x2-= this.velocidad;
//					
//					this.x2 = this.x1 + 120;
//					
//					this.path = run[0];
//					
//					this.direccion = "L";
//					
//				}
//				
//				if (ubiX > this.x2) {
//					this.x1+= this.velocidad;
//					this.x2+= this.velocidad;
//					
//					this.x2 = this.x1 + 120;
//					
//					this.path = run[1];
//					
//					this.direccion = "R";
//				}
//				
//				if (ubiY < this.y2) {
//					this.y1-= this.velocidad;
//					this.y2-= this.velocidad;
//				}
//				
//				if (ubiY > this.y2) {
//					this.y1+= this.velocidad;
//					this.y2+= this.velocidad;
//				}
//				
//			}
//		
//		}
		
//		System.out.println(this.mover);
		
		/*SI SE ACTIVA EL LOCKSCROLL AL PERSONAJE*/
		
		//DERECHA
//		if (this.x2 > w.getSize().width * (pantX+1)) {
//			pantX++;
//			f.scroll(-w.getSize().width, 0);
//		}
//		
//		//IZQUIERDA
//		if (this.x1 < w.getSize().width * (pantX)) {
//			pantX--;
//			f.scroll(w.getSize().width, 0);
//		}
//		
//		//TOP
//		if (this.y1 < w.getSize().height * (pantY)) {
//			pantY--;
//			f.scroll(0, w.getSize().height);
//		}
////		
//		//BOTTOM
//		if (this.y2 > w.getSize().height * (pantY+1)) {
//			pantY++;
//			f.scroll(0, -w.getSize().height);
//		}
//		
//		if (this.scroll) {
//			f.scroll(0, 2);
//		}

	}

	public void infoMouse() {

		System.out.println("-------------------------------");
		System.out.println(pantX);
		System.out.println(pantY);
		System.out.println("width: " + w.getSize().width);
		System.out.println("height: " + w.getSize().height);
		System.out.println("has clicat al pixel en X (scroll): " + f.getMouseX());
		System.out.println("has clicat al pixel en Y (scroll): " + f.getMouseY());
		System.out.println("has clicat al pixel en X (window): " + ((int) f.getMouseX() + ((int) pantX * (int) w.getSize().width)));
		System.out.println("has clicat al pixel en Y (window): " + ((int) f.getMouseY() + ((int) pantY * (int) w.getSize().height)));
		System.out.println("-------------------------------");
		
	}
}