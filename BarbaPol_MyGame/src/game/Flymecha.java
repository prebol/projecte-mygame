package game;

import Core.Field;
import Core.Window;

public class Flymecha extends Boss {
	
	PersonalTimerTask bajar = new PersonalTimerTask() {
		
		public void run () {
			setVelocity(0, 0);
			bajar.reset();
		}
		
	};
	
	PersonalTimerTask subir = new PersonalTimerTask() {
		
		public void run () {
			setVelocity(0, 0);
			bajar.reset();
		}
		
	};
	
	PersonalTimerTask morir = new PersonalTimerTask() {
		
		public void run () {
			delete();
			morir.reset();
		}
		
	};
	
	Window w;
	
	int vida = 500;

	public Flymecha(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, Window w) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.vivo = true;
		this.trigger = true;
		this.w = w;
	}

	public void bajar() {
		
		this.setVelocity(0, 4);
		this.timer.schedule(bajar, 1500);
		
	}
	
	public void subir() {
		
		this.setVelocity(0, -4);
		this.timer.schedule(subir, 1500);
		
	}

	@Override
	public void danyar(int damage) {
		this.vida-= damage;
		if (this.vida <= 0) {
			System.out.println("MUEREEEEEEEEEEEE");
			this.morir();
		}
		
	}
	
	@Override
	public void morir() {
		
		this.vivo = false;
		this.path = "rsc/graphic/NPCs/enemies/boss robot/explosionFly.gif";
		timer.schedule(morir, 200);
		morir.reset();
		
	}
	
	public void update() {
		
		if (!this.vivo) {
			
		}
		
	}
	

}
