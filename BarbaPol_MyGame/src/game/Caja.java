package game;

import Core.Field;
import Core.Sprite;

public class Caja extends Sprite {
	
	Field f;

	static boolean habilitat = true;
	
	public Caja(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.f = f;
	}
	
	public void update() {
		
	}

}
