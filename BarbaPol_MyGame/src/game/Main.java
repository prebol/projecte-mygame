package game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.event.MenuKeyEvent;

import Core.Field;
import Core.Sprite;
import Core.Window;

import java.awt.Color;

public class Main {

	//static Field f = new Field();
	//static Field f2 = new Field();
	
	//ARRAY QUE CONTE TOTS ELS FIELDS UTILITZATS
	public static Field [] fs = new Field[4];
	
	//FIELD QUE SEMPRE ESTA DINTRE DEL WINDOW
	static Field ac = null;
	
	//WINDOW
	public static Window w = null;
	
	static int currentMapId = 2;
	
	//BOOLEAN PARA SABER SI SE HA ACABADO LA PARTIDA
	static Boolean sortir = false;
	
	static Jugador cyborg;
	
	static Personaje player;
	
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		
		//FIELD 1 - BOSS OJO
		 fs[0] = new Field();
		 
		 //FIELD 2 - MAPA LIBRE
		 fs[1] = new Field();
		 
		 //FIELD 3 - MENU
		 fs[2] = new Field();
		 
		//FIELD 4 - BOSS ROBOT
		 fs[3] = new Field();
		
		//FIELD ACTUAL
		ac = fs[currentMapId];
		
		w = new Window(ac);
		
		int altura = w.getSize().height;
		
		int base = w.getSize().width;
		
		/*-----------------*/
		/*-----FIELD 1-----*/
		/*-----------------*/
		
//		ArrayList<Integer> posJ = cargarJugador();
		
//		Jugador cyborg = (Jugador) crearJugador(posJ);
		
		/*-----JUGADOR-----*/
		/*
		 * PEQUEÑO: 100, 100, 160, 205
		 * MEDIANO: 
		 * 
		 */
		Field f = new Field();
		cyborg = new Jugador("Cyborg", 100, 100, 160, 205, 0, "rsc/graphic/jugador/player_idle_R.gif", f, w);
		
		
		Projectil blue = new Projectil("Blue Laser", 0, 0, 0, 0, 0, "rsc/graphic/projectil/blueprojectile.gif", fs[3], w);
		blue.damage = 20;
		blue.orderInLayer = 20;
//		blue.colorSprite = true;
		
		Projectil bomb = new Projectil("BOMB Parabola", 0, 0, 0, 0, 0, "rsc/graphic/projectil/rasengan.gif", fs[0], w);
		bomb.damage = 20;
		
		cyborg.p = blue;
		
		/*-----NPCs-----*/
		
		GiagantEye mamabicho = new GiagantEye("Enemigo 1", base-200, altura-700, base, altura-400, 0, "rsc/graphic/NPCs/enemies/boss_ojo/ojo_boss.gif", fs[0]);
		
		mamabicho.vida = 100;
		
		mamabicho.collisionBox = true;
		mamabicho.drawingBoxExtraLeft = 80;
		mamabicho.drawingBoxExtraUp = 20;
		mamabicho.drawingBoxExtraDown = 20;
		
//		mamabicho.colorSprite = true;
		
		mamabicho.p = bomb;
		
		/*-----MAPA-----*/
		
		Objeto suelo = new Objeto("suelo", 0, altura-170, base, altura, 0, "rsc/graphic/suelo.png", fs[0]);
		
		Objeto pared_R = new Objeto("Pared", base-50, 0, base+200, altura, 0, "rsc/graphic/suelo2.png", fs[0]);
		Objeto pared_L = new Objeto("Pared", -200, 0, 15, altura, 0, "rsc/graphic/suelo2.png", fs[0]);
		
		fs[0].background = "rsc/graphic/background/subground.png";
		
		/*------------------------------*/
		/*-----FIELD 2 - OVERWORLD -----*/
		/*------------------------------*/
		
		
		
//		Objeto suelo2 = new Objeto("Suelo", 0, 0, 1000, 1000, 0, "", fs[1]);
//		suelo2.colorSprite = true;
//		suelo2.solid = true;
		
		Fondo fondo1 = new Fondo("sus", -1920, -1080, 5760, 3240, 0, "rsc/graphic/background/sus.jpg", fs[1]);
		fondo1.orderInLayer = 1;
		
		/*-----PERSONAJE-----*/
		/*
		 * PEQUEÑO: 100, 100, 175, 240
		 * MEDIANO: 
		 * 
		 */
		
		player = new Personaje("player", 1106, -657, 1181, -517, 0, "rsc/graphic/playerMAP/player_idle_R.gif", fs[1], w);
		player.orderInLayer = 20;
		player.collisionBox = true;
//		player.colorSprite = true;
		
		//FIJAR SCROLL A PERSONAJE
		fs[1].lockScroll(player, w);
		
		//CANVIA EL COLOR DEL FIELD A NEGRE
		fs[1].setBackground(new Color(0));
		
		Evento boss1 = new Evento("boss1", -70, 2391, 107, 2623, 0, "rsc/graphic/suelo.png", fs[1]);
//		boss1.colorSprite = true;
//		boss1.color = Color.yellow;
		boss1.angle = 45;
		boss1.orderInLayer = 30;
		
		Extra F = new Extra("F", -50, 2350, 50, 2450, 0, "rsc/graphic/background/F.png", fs[1]);
		F.orderInLayer = 30;
		
		Objeto mapa = new Objeto("tablaMapa", 2298, 2287, 2601, 2421, 0, "", fs[1]);
//		mapa.colorSprite = true;
//		mapa.color = Color.yellow;
//		mapa.solid = true;
		mapa.orderInLayer = 30;
		
//		Objeto suelo2 = new Objeto("Suelo", 0, altura-170, base, altura, 0, "rsc/graphic/suelo.png", fs[1]);
//		
//		Objeto pared_R2 = new Objeto("Pared", base-50, 0, base+200, altura, 0, "rsc/graphic/suelo2.pngXD", fs[1]);
//		Objeto pared_L2 = new Objeto("Pared", -200, 0, 15, altura, 0, "rsc/graphic/suelo2.pngXD", fs[1]);
		
//		fs[1].background = "rsc/graphic/background/sus.jpg";
		
		/*-------------------------*/
		/*-----FIELD 3 - MENU -----*/
		/*-------------------------*/
		
		/*
		 * Calcular posicion y tamaño de los elementos del menu
		 * 
		 */
		
		/*---WIDTH DE LOS ELEMENTOS---*/
		Double TitleW = 1.2;
		int titleW1 = (base/2) - ((int) ((base/10)*TitleW));	//Quanto se aleja del centro de la ventana (Left)
		int titleW2 = (base/2) + ((int) ((base/10)*TitleW)); //Quanto se aleja del centro de la ventana (Right)
		
		Double CajaW = 1.2;
		int cajaW1 = (base/2) - ((int) ((base/10)*CajaW));	//Quanto se aleja del centro de la ventana (Left)
		int cajaW2 = (base/2) + ((int) ((base/10)*CajaW));	//Quanto se aleja del centro de la ventana (Right)
		
		/*---NIVEL DE ALTURA DE LOS ELEMENTOS---*/
		
		Double titleH = 2.0;
		int title_HInici = (altura/10);
		int title_HFinal = (altura/10)+ (int) ((altura/10)*titleH);
		
		Double cajaH = 1.2;
		
		int nuevaG = 3;
		int nueva_HInici = (altura/10) * nuevaG;							//La caja partirá del 10% de la ventana * num (30%)
		int nueva_HFinal = nueva_HInici + ((int) ((altura/10)*cajaH));	//La caja acabará en la altura de "nueva_HInici" + num
		
		int cargarG = 5;
		int cargar_HInici = (altura/10) * cargarG;								//La caja partirá del 10% de la ventana * num
		int cargar_HFinal = cargar_HInici + ((int) ((altura/10)*cajaH));	//La caja acabará en la altura de "cargar_HInici" + num
		
		int salirG = 7;
		int salir_HInici = (altura/10) * salirG;								//La caja partirá del 10% de la ventana * num
		int salir_HFinal = salir_HInici + ((int) ((altura/10)*cajaH));	//La caja acabará en la altura de "salir_HInici" + num
		
		/*--------------------------------------*/
		
		Caja title = new Caja("title", titleW1, title_HInici, titleW2, title_HFinal, 0, "rsc/graphic/menu/TITLE.png", fs[2]);
		
		Caja nueva = new Caja("nueva", cajaW1, nueva_HInici, cajaW2, nueva_HFinal, 0, "rsc/graphic/menu/cartel_nueva.png", fs[2]);
		
		Caja cargar = new Caja("cargar", cajaW1, cargar_HInici, cajaW2, cargar_HFinal, 0, "rsc/graphic/menu/cartel_cargar.png", fs[2]);
		
		Caja salir = new Caja("salir", cajaW1, salir_HInici, cajaW2, salir_HFinal, 0, "rsc/graphic/menu/cartel_salir.png", fs[2]);
		
		
//		Caja title = new Caja("title", (base/2) - (int) ((base/10)*1.2), (altura/10), (base/2)+ (int) ((base/10)*1.2), (altura/10)+ (int) ((altura/10)*2), 0, "rsc/graphic/menu/TITLE.png", fs[2]);
//		
//		Caja nueva = new Caja("nueva", (base/2)-(int) ((base/10)*1.2), (altura/10)*3, (base/2)+(int) ((base/10)*1.2), ((altura/10)*3) + (int) ((altura/10)*1.2), 0, "rsc/graphic/menu/cartel_nueva.png", fs[2]);
//		
//		Caja cargar = new Caja("cargar", (base/2)-(int) ((base/10)*1.2), (altura/10)*5, (base/2)+(int) ((base/10)*1.2), ((altura/10)*5) + (int) ((altura/10)*1.2), 0, "rsc/graphic/menu/cartel_cargar.png", fs[2]);
//		
//		Caja salir = new Caja("salir", (base/2)-(int) ((base/10)*1.2), (altura/10)*7, (base/2)+(int) ((base/10)*1.2), ((altura/10)*7) + (int) ((altura/10)*1.2), 0, "rsc/graphic/menu/cartel_salir.png", fs[2]);
		
		fs[2].background = "rsc/graphic/background/menu.jpg";
		
		
//		subground.png
//		temple.jpg
//		templeIN.jpg
		
		/*----------------*/
		/*-----PROVES-----*/
		/*----------------*/
		
		//POP UP CON INPUT
//		String resposta = w.showInputPopup("FUNCIONA??????");
//		System.out.println(resposta);
		
		//POP UP NORMAL
//		w.showPopup("SORPRESA!!!");
		
		//LABEL PARA INFO
//		String[] etiquetes = new String[3];
//		etiquetes[0] = "POS X1: " + Main.cyborg.x1;
//		etiquetes[1] = "UN LABEL";
//		etiquetes[2] = "LETS GO";
// 		
//		w.setActLabels(true);
//		
//		w.setLabels(etiquetes);
		
		//SONIDOS CONF
//		w.playMusic("rsc/sound/music/versos_perversos.wav");
		w.sfxVolume = 65;
		
		while (!sortir) {
			
			fs[currentMapId].draw();
            
            Thread.sleep(25);
            
            /*MENU*/
            if (ac == fs[2]) {
            	 mouseOver();
                 mouseOut();
                 mouseClick();
                 
//                 w.playMusic("");
            }
            
           /*BOSSES*/
           if (ac == fs[0] || ac == fs[3]) {
        	   input(cyborg);
        	   
//        	   w.playMusic("");
           }
           
           /*BOSS ROBOT*/
//           if (ac == fs[3]) {
//        	   input(cyborg3);
//           }
           
           /*OVERWORLD*/
           if (ac == fs[1] && currentMapId == 1) {
        	   inputP(player);
        	   
//        	   w.playMusic("");
           }
            
            
		}
		
		System.out.println("FIN DEL JUEGO");
		
		
	}

	/**
	 * Detecta sobre quins elements (sprites) NO esta a sobra el ratoli per a poder posar la imatge per defecte
	 */
	
	private static void mouseOut() {

		ArrayList<Sprite> listaSpritesOver = fs[2].getMouseOverSprite();
		
		for(Sprite s : fs[2].getSprites()) {
			if (!listaSpritesOver.contains(s)) {
				switch(s.name) {
				case "nueva":
					s.path = "rsc/graphic/menu/cartel_nueva.png";
					break;
				case "cargar":
					s.path = "rsc/graphic/menu/cartel_cargar.png";
					break;
				case "salir":
					s.path = "rsc/graphic/menu/cartel_salir.png";
					break;
				}
			}
		}
		
		
	}
	
	/**
	 * Detecta sobre quin element (sprite) esta a sobra el ratol� i resaltar la imatge
	 */

	private static void mouseOver() {

		ArrayList<Sprite> listaSpritesOver = fs[2].getMouseOverSprite();
		for(Sprite s : listaSpritesOver) {
			Caja nueva = (Caja) s;
			
			if (nueva.habilitat) {
				switch(s.name) {
				case "nueva":
					s.path = "rsc/graphic/menu/cartel_nueva_hover.png";
					break;
				case "cargar":
					s.path = "rsc/graphic/menu/cartel_cargar_hover.png";
					break;
				case "salir":
					s.path = "rsc/graphic/menu/cartel_salir_hover.png";
					break;
				}
			} else {
				System.out.println("ESTAS EN PANEL DE NUEVA");
			}
			
			
		}
		
	}
	
	/**
	 * Detecta quin element (sprite) del menu s'ha clicat, fa una acci� corresponent a cada opci�
	 */

	private static void mouseClick() {
		
		ArrayList<Sprite> listaSpritesClickados = fs[2].getCurrentMouseSprite();
		for(Sprite s : listaSpritesClickados) {
			Caja nueva = (Caja) s;
			
			if (nueva.habilitat) {
				switch(s.name) {
				case "title":
					System.out.println("ARNAU Y TEORIAS ES EL MEJOR SHIPEO DE LA HISTORIA");
					break;
				case "nueva":
//					System.out.println("nueva");
//					nueva.habilitat = false;
//					Caja hola = new Caja("mensaje", (w.getSize().width/2)-350, (w.getSize().height/2)-300, (w.getSize().width/2)+350, (w.getSize().height/2)+300, 0, "rsc/graphic/menu/panel.png", fs[2]);
////					for (Sprite sCaja: fs[2].getSprites()) {
////						sCaja.visible = false;
////					}
					newGame();
					break;
				case "cargar":
					cargarPartida();
					
					break;
				case "salir":
					w.stopMusic();
					w.close();
					Main.sortir = true;
					break;
				}
			} else {
				System.out.println("ESTAS EN PANEL NUEVA");
			}
			
			
		}

	}

	private static Jugador crearJugador(ArrayList<Integer> posJ) {
		
		if (posJ == null) {
			Jugador cyborg = new Jugador("Cyborg", 100, 100, 160, 205, 0, "rsc/graphic/jugador/player_idle_R.gif", ac, w);
			
			return cyborg;
		} else {
			Jugador cyborg = new Jugador("Cyborg", posJ.get(0), posJ.get(1), posJ.get(2), posJ.get(3), 0, "rsc/graphic/jugador/player_idle_R.gif", ac, w);
			
			return cyborg;
		}
		
	}
	
	/*///////////////////////////*/
	/*///INPUT JUGADOR IN-GAME///*/
	/*///////////////////////////*/

	/**
	 * MOVIMIENTO DE UN OBJETO DE TIPO "JUGADOR" (FIELDS PARA PELEAS)
	 * @param cyborg
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */

	public static void input(Jugador cyborg) throws IOException, ClassNotFoundException {
		
		
		if (cyborg.vivo && cyborg.visible) {
			if((w.getPressedKeys().contains('a') || w.getPressedKeys().contains('A'))) {
				cyborg.moviment(Input.ESQUERRA);
			} else if ((w.getPressedKeys().contains('d') || w.getPressedKeys().contains('D'))) {
				cyborg.moviment(Input.DRETA);
			} else {
				cyborg.quieto();
			}
			
			if ((w.getPressedKeys().contains('w') || w.getPressedKeys().contains('W'))) {
				cyborg.arriba = true;
			} else {
				cyborg.arriba = false;
			}

//			System.out.println("GETPRESSEDKEYS: \n" + w.getPressedKeys());
//			System.out.println("GETPRESSEDKEYS: \n" + w.getPressedKeys().contains(Character.(72));
//			System.out.println(w.getKeysDown());
			
			//SALTAR
			if (w.getKeysDown().contains(' ')) {
				cyborg.saltar(Input.SALTAR);
			}
			
			//DISPARAR
			if (w.getPressedKeys().contains('L') || w.getPressedKeys().contains('l')) {
				cyborg.disparar(Input.DISPARA);
			}
			
			//DASH
			if (w.getKeysDown().contains('k') || w.getKeysDown().contains('K')) {
				cyborg.dash(Input.DASH);
			}
			
//			System.out.println(cyborg.collidesWithTriggerInField(fs[3]));
			
			/*INTEREACCIONAR CON EVENTOS*/
			if (w.getKeysDown().contains('q') || w.getKeysDown().contains('Q')) {
				ArrayList<Sprite> touching = cyborg.collidesWithTriggerInField(fs[3]);
//				System.out.println(touching);
				for (Sprite s: touching) {
					if (s.name.equals("portal")) {
						
						if (currentMapId == 1) {
							System.out.println("El Field 1 ja esta carregat");
						} else {
							fs[currentMapId].removeSprite(cyborg);
							currentMapId = 1;
							changeMap();
						}
						
					} else {
						System.out.println("NADA CON QUE INTERACTUAR");
					}
					
				}
				
			}
			
			/*-----NO ESTARAN ACTIVAS EN LA VERSION FINAL-----*/
			
			//TP A 0, 0
			if (w.getKeysDown().contains('g') || w.getKeysDown().contains('G')) {
				cyborg.tpPlayer();
			}
			/*
			//GUARDAR PARTIDA
			if (w.getKeysDown().contains('n') || w.getKeysDown().contains('N')) {
				cyborg.guardarJugador();
			}
			
			//CARGAR PARTIDA
			if (w.getKeysDown().contains('m') || w.getKeysDown().contains('M')) {
				cyborg.cargarJugador();
			}
			*/
			//FIELD 1 - BOSS OJO
//			if (w.getKeysDown().contains('r') || w.getKeysDown().contains('R')) {
//				if (currentMapId == 0) {
//					System.out.println("El Field 1 ja esta carregat");
//				} else {
//					fs[currentMapId].removeSprite(cyborg);
//					currentMapId = 0;
//					cyborg.setField(fs[currentMapId]);
//					changeMap();
//				}
//				
//			}
			
			//FIELD 2 - MENU
			/*
			if (w.getKeysDown().contains('f') || w.getKeysDown().contains('F')) {
				if (currentMapId == 2) {
					System.out.println("El Field 2 ja esta carregat");
				} else {
					fs[currentMapId].removeSprite(cyborg);
					currentMapId = 2;
					changeMap();
				}
				
			}
			
			if (w.getKeysDown().contains('p') || w.getKeysDown().contains('P')) {
				cyborg.spawnearBoss();
			}
			*/
//			if (w.getKeysDown().contains('t') || w.getKeysDown().contains('T')) {
//				w.showPopup("SORPRESA!!!");
//			}
			
//			if (fs[0].getCurrentMouseX() != -1) {
//				cyborg.disparar(Input.DISPARA);
//			}
		} else {
			System.out.println("Jugador esta mort");
		}
		
	}
	
	/*///////////////////////////////*/
	/*///INPUT PERSONAJE OVERWORLD///*/
	/*///////////////////////////////*/
	
	public static void inputP (Personaje p) {
//		System.out.println(w.getPressedKeys());
		if (w.getPressedKeys().size() != 0) {
			if (w.getPressedKeys().size() != 0) {
				if((w.getPressedKeys().contains('a') || w.getPressedKeys().contains('A'))) {
					p.moviment(InputP.LEFT);
				} else if ((w.getPressedKeys().contains('d') || w.getPressedKeys().contains('D'))) {
					p.moviment(InputP.RIGHT);
				}
				
				if ((w.getPressedKeys().contains('w') || w.getPressedKeys().contains('W'))) {
					p.moviment(InputP.TOP);
				} else if ((w.getPressedKeys().contains('s') || w.getPressedKeys().contains('S'))) {
					p.moviment(InputP.BOTTOM);
				}
			} else {
				p.quieto();
			}
			
			/*INTEREACCIONAR CON EVENTOS*/
			if ((w.getKeysDown().contains('f') || w.getKeysDown().contains('F'))) {
				ArrayList<Sprite> touching = p.collidesWithTriggerInField(fs[1]);
//				System.out.println(touching);
				for (Sprite s: touching) {
					if (s.name.equals("boss1")) {
						
						currentMapId = 3;
						anadirFieldCyborg();
						generarBossRobot();
						changeMap();
						
						//CALCULO PANEL EN SCROLL NO FIJADO AL PERSONAJE (X PANTALLAS)
//						Caja hola = new Caja("mensaje", ((w.getSize().width * (p.pantX+1)) - (w.getSize().width/2))-350, ((w.getSize().height*(p.pantY+1))-(w.getSize().height/2))-300, ((w.getSize().width * (p.pantX+1))-(w.getSize().width/2))+350, ((w.getSize().height*(p.pantY+1))-(w.getSize().height/2))+300, 0, "rsc/graphic/menu/panel.png", fs[1]);
						
						//CALCULO PANEL EN SCROLL FIJADO AL PERSONAJE
//						int midX = p.x2 - ((p.x2 - p.x1)/2);
//						int midY = p.y2 - ((p.y2 - p.y1)/2);
//						
//						Caja hola = new Caja("mensaje", p.x1-250, p.y1-300, p.x1+250, p.y1+300, 0, "rsc/graphic/menu/panel.png", fs[1]);
//						hola.orderInLayer = 40;
//						
//						Caja luchar = new Caja("luchar", p.x1-150, p.y2+50, p.x1-50, p.y2+150, 0, "rsc/graphic/menu/panel.png", fs[1]);
////						luchar.colorSprite = true;
////						luchar.color = Color.red;
//						luchar.orderInLayer = 50;
//						
//						Caja salir = new Caja("salir", p.x1+50, p.y2+50, p.x1+150, p.y2+150, 0, "rsc/graphic/menu/panel.png", fs[1]);
////						salir.colorSprite = true;
////						salir.color = Color.white;
//						salir.orderInLayer = 50;
//						
//						p.mover = true;
						
//						boolean sortir = false;
//						
//						while (!sortir) {
//							
//							sortir = mouseClickBOSS();
//							
//						}
						
						System.out.println("estas tocando el cartel del boss 1");
					} else {
						System.out.println("NADA CON QUE INTERACTUAR");
					}
					
				}
			}
			
			//GUARDAR
			if ((w.getKeysDown().contains('n') || w.getKeysDown().contains('N'))) {
				guardarPartida();
			}
			
			/*//////////////////////////*/
			
			//FIELD 1 - BOSS OJO
			if (w.getKeysDown().contains('r') || w.getKeysDown().contains('R')) {
				if (currentMapId == 0) {
					System.out.println("El Field 1 ja esta carregat");
				} else {
					currentMapId = 0;
					anadirFieldCyborg();
					changeMap();
				}
				
			}
			
			//FIELD 2 - MENU
			if (w.getKeysDown().contains('')) {
				if (currentMapId == 2) {
					System.out.println("El Field 2 ja esta carregat");
				} else {
					fs[currentMapId].removeSprite(cyborg);
					currentMapId = 2;
					changeMap();
				}
			}
			
			if (w.getKeysDown().contains('c') || w.getKeysDown().contains('C')) {
				p.scroll = true;
			}
			
			if (w.getKeysDown().contains('x') || w.getKeysDown().contains('X')) {
				p.scroll = false;
			}
			
		} else {
			p.quieto();
		}
		
//		if (fs[1].getCurrentMouseX() != -1) {
//			p.infoMouse();
//			
//			p.posMX = fs[1].getMouseX();
//			p.posMY = fs[1].getMouseY();
////			p.andar();
//		}
		
	}
	
	private static void guardarPartida() {

		try {
			//FITXER JUGADOR
	        File fJ = new File("rsc/files/jugador.bin");
	        FileOutputStream fosJ = new FileOutputStream(fJ, false);
	        ObjectOutputStream oosJ = new ObjectOutputStream(fosJ);
			
	        oosJ.writeInt(player.x1);
	        oosJ.writeInt(player.y1);
	        oosJ.writeInt(player.x2);
	        oosJ.writeInt(player.y2);

	        oosJ.close();
	        
	        w.showPopup("PARTIDA GUARDADA");
	        
		} catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("FileNotFoundException FFFFFFFFFFFFFFF");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IOException FFFFFFFFFFFFFFF");
        }
		
	}
	
	private static void cargarPartida() {
		
		try {
			
			currentMapId = 1;
			changeMap();
			
			File fJ = new File("rsc/files/jugador.bin");
	        FileInputStream fisJ = new FileInputStream(fJ);
	        ObjectInputStream oisJ = new ObjectInputStream(fisJ);
	        
	        player.x1 = oisJ.readInt();
	        player.y1 = oisJ.readInt();
	        player.x2 = oisJ.readInt();
	        player.y2 = oisJ.readInt();
	        
	        //this.mapid = ois.readInt();
//	        System.out.println(ois.readInt());
//	        System.out.println(ois.readInt());
//	        System.out.println(ois.readInt());
//	        System.out.println(ois.readInt());
	        
	        oisJ.close();
	        
		} catch (FileNotFoundException e) {
            System.out.println("no existeix el fitxer");
            e.printStackTrace();
            
            currentMapId = 2;
			changeMap();
			w.showPopup("NO HAY DATOS GUARDADOS");
            
        } catch (IOException e) {
            System.out.println("excepció d'entrada/sortida");
            e.printStackTrace();
        }
		
	}

//	private static boolean mouseClickBOSS() {
//		
//		ArrayList<Sprite> listaSpritesClickados = fs[1].getCurrentMouseSprite();
//		for(Sprite s : listaSpritesClickados) {
//			Caja nueva = (Caja) s;
//			
//			switch(s.name) {
//			case "luchar":
////				currentMapId = 3;
////				a�adirFieldCyborg();
////				generarBossRobot();
////				changeMap();
//				System.out.println("LUCHAAAAAAAAARRRRRRRRRRRRRRRRRR");
//				break;
//			case "salir":
////				System.out.println("nueva");
////				nueva.habilitat = false;
////				Caja hola = new Caja("mensaje", (w.getSize().width/2)-350, (w.getSize().height/2)-300, (w.getSize().width/2)+350, (w.getSize().height/2)+300, 0, "rsc/graphic/menu/panel.png", fs[2]);
//////					for (Sprite sCaja: fs[2].getSprites()) {
//////						sCaja.visible = false;
//////					}
////				newGame();
//				System.out.println("SALIIIIIIIIIIIIIIIIIIIIRRRRRRRR");
//				break;
//			}
//			
//			
//		}
//		
//		return false;
//
//	}
	
	private static void anadirFieldCyborg() {

		cyborg.setField(fs[currentMapId]);
		cyborg.x1 = cyborg.iniX1;
		cyborg.y1 = cyborg.iniY1;
		cyborg.x2 = cyborg.iniX2;
		cyborg.y2 = cyborg.iniY2;
		cyborg.vivo = true;
		cyborg.path = cyborg.idle[1];
		UI.getInstance().vidas.vidas = 4;
		cyborg.p.setField(fs[currentMapId]);
		
	}

	public static void changeMap () {

		ac = fs[currentMapId];
		w.changeField(ac);
		w.setExtendedState(JFrame.MAXIMIZED_BOTH);
			
	}
	

	public static void newGame() {
		
		File f = new File("rsc/files/jugador.bin");
		
		f.delete();
		
		w.showPopup("SE HAN BORRADO LOS DATOS GUARDADOS");
		
		currentMapId = 1;
		player.x1 = player.iniX1;
		player.y1 = player.iniY1;
		player.x2 = player.iniX2;
		player.y2 = player.iniY2;
		player.path = player.idle[1];
		changeMap();
		
	}
	
	private static void generarBossRobot() {
		/*-----------------*/
		/*-----FIELD 4 - BOSS ROBOT-----*/
		/*-----------------*/
	
		/*JUGADOR*/
		
//		Jugador cyborg3 = new Jugador("Cyborg", 100, 100, 160, 205, 0, "rsc/graphic/jugador/player_idle_R.gif", fs[3], w);
//		
////		cyborg3.colorSprite = true;
//		cyborg3.orderInLayer = 60;
//		cyborg.layer = 50;
//		
//		Projectil blue3 = new Projectil("Blue Laser", 0, 0, 0, 0, 0, "rsc/graphic/projectil/blueprojectile.gif", fs[3], w);
//		blue3.damage = 20;
//		
//		cyborg3.p = blue3;
		
		/*ENEMIES*/
		
//		Extra crab2 = new Extra("crabmecha", 700, altura-575, 1200, altura-200, 0, "rsc/graphic/NPCs/enemies/boss robot/Crabmecha.gif", fs[3]);
		
		UI.instance = null;
		
		fs[3].clear();
		
		UI.getInstance();
		
		w.playMusic("rsc/sound/music/gta1.wav");
		
		cyborg.visible = true;
		cyborg.flippedX = false;
		
		int altura = Main.w.getSize().height;
		int base = Main.w.getSize().width;
		
		//CRABMECHA
		Crabmecha crabmecha = new Crabmecha("crabmecha", 1450, altura-575, 1650, altura-200, 0, "rsc/graphic/NPCs/enemies/boss robot/Crabmecha.gif", fs[3], w);
//		crabmecha.vivo = false;
		
		//COHETE
		Cohete cohete = new Cohete("cohete", 0, 0, 0, 0, 0, "rsc/graphic/projectil/cohete.png", fs[3], w);
		cohete.enemigo = true;
		cohete.orderInLayer = 40;
		
		crabmecha.p = cohete;
		
		crabmecha.orderInLayer = 50;
		crabmecha.collisionBox = true;
		crabmecha.drawingBoxExtraLeft = 150;
		crabmecha.drawingBoxExtraRight = 150;
		
		//BOMBA
		//	width: 200 height: 125
		Bomba bomba = new Bomba("bomba", 0, 0, 0, 0, 0, "rsc/graphic/projectil/barrido.gif", fs[3]);
		bomba.circle = true;
		
		bomba.collisionBox = true;
		bomba.drawingBoxExtraLeft = 25;
		bomba.drawingBoxExtraRight = 25;
		bomba.drawingBoxExtraUp = 20;
		bomba.orderInLayer = 30;
		
		bomba.trigger = true;
		
		crabmecha.bomba = bomba;
		
		/*EXTRA*/
		
		Extra topShip = new Extra("topShip", 0, 0, base, 300, 0, "rsc/graphic/NPCs/enemies/boss robot/topShip.gif", fs[3]);
		topShip.orderInLayer = 10;
		
		crabmecha.topShip = topShip;
		
		/*-----MAPA-----*/
		
		Objeto suelo3 = new Objeto("suelo", 0, altura-170, base, altura, 0, "rsc/graphic/suelo.png", fs[3]);
//		suelo3.colorSprite = true;
		suelo3.color = new Color(47,79,79);
		suelo3.orderInLayer = 90;
		
		Objeto pared_R3 = new Objeto("Pared_R", base-50, 0, base+200, altura, 0, "rsc/graphic/suelo2.pngXD", fs[3]);
		Objeto pared_L3 = new Objeto("Pared_L", -200, 0, 15, altura, 0, "rsc/graphic/suelo2.pngXD", fs[3]);
//		pared_R3.colorSprite = true;
//		pared_L3.colorSprite = true;
		
		fs[3].background = "rsc/graphic/background/graveyard.jpg";
		
	}
	
}
