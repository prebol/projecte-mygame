package game;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Objeto extends PhysicBody {

	public Objeto(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.solid = true;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {

		System.out.println(this.name + " HA CHOCADO CON ALGO");
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {

		System.out.println(this.name + " HA PARADO DE CHOCAR CON ALGO");
		
	}
	
	@Override
	public void onTriggerEnter(Sprite sprite) {
		
		System.out.println("OBJETO HA CHOCADO CON ALGUN TRIGGER");
		
	}

//	public Objeto(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
//		super(name, x1, y1, x2, y2, angle, path, f);
//		this.solid = true;
//	}

	
}
