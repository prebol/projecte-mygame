package game;

import Core.Field;
import Core.Sprite;

public class Vidas extends Sprite {
	
	static int vidas = 4;
	
	String [] img = new String[] {"rsc/graphic/UI/healthbar/green/healthbar_0.png",	//VIDA 4 BARRAS
								"rsc/graphic/UI/healthbar/green/healthbar_1.png",	//VIDA 3 BARRAS
								"rsc/graphic/UI/healthbar/green/healthbar_2.png",	//VIDA 2 BARRAS
								"rsc/graphic/UI/healthbar/green/healthbar_3.png",	//VIDA 1 BARRAS
								"rsc/graphic/UI/healthbar/green/healthbar_4.png"};	//VIDA 0 BARRAS

	public Vidas(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.solid = false;
		this.path = "rsc/graphic/UI/healthbar/green/healthbar_4.png";
	}
	
	public void update () {
		if (vidas == 4) {
			this.path = img[4];
		} else if (vidas == 3) {
			this.path = img[3];
		} else if (vidas == 2) {
			this.path = img[2];
		} else if (vidas == 1) {
			this.path = img[1];
		} else if (vidas == 0) {
			this.path = img[0];
		}
	}

}
