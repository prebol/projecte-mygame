package game;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Bomba extends PhysicBody {
	
	boolean derecha;

	public Bomba(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		// TODO Auto-generated constructor stub
	}
	
	public Bomba(Bomba b, int x1, int y1, int x2, int y2, double angle) {
		super(b.name, x1, y1, x2, y2, angle, b.path, b.f);
		this.circle = true;
		this.collisionBox = b.collisionBox;
		this.drawingBoxExtraLeft = b.drawingBoxExtraLeft;
		this.drawingBoxExtraRight = b.drawingBoxExtraRight;
		this.drawingBoxExtraUp = b.drawingBoxExtraUp;
		this.orderInLayer = b.orderInLayer;
		this.trigger = b.trigger;
		
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {

		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void update() {
		
		if (this.derecha) {
			this.setVelocity(20, 0);
		} else {
			this.setVelocity(-20, 0);
		}
		
		if (this.x2 < 0 || this.x1 > Main.w.getSize().width) {
			this.delete();
		}
		
		
	}

	

}
